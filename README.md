
# Jackson annotations example

## Introduction

If you only touch JSON serialisation from time to time, it is easy to forget the little things.
This project aims to demonstrate the simplest aspects involved with plain old java object (POJO) modeling
and its serialisation and deserialisation using Jackson's JSON.

It achieves this by using very basic concepts: **family**, **person**, **pet** and **address**. A family lives
at an address. It comprises a number of people, and there are pets of different types (either cats or dogs).

This simple model demonstrates the following modeling concepts:

* Serialisation of regular objects.
    * A Person has a name, an age and a gender.
* Serialisation of complex enums.
    * A Gender enum is used to determine a code, the adult term (man/woman) and the child term (boy/girl).
* Serialisation of generics/collections.
    * A Family is a List of Person.
* Serialisation of abstract classes.
    * A Pet can be a Dog or a Cat (both of which are derived from an abstract base class).
    * A Dog could be referred to as a 'dog' or a 'hound' (i.e. an 'hound' is an alias for 'dog').
    * A Family can also have a List of Pet.

**For the impatient**, see: `com.darkmine.jackson.exmaple.model.impl.FamilyImpl.java`

## Key points

* Don't annotate interfaces - only annotate the implementation classes.
* I'm not really a fan of the "Impl" naming convention.
    * It's here to clearly distinguish the objects and their use.
* Interfaces can be shared across projects, implementations might differ:
    * Mongo documents objects on the server-side.
    * Data transfer objects (DTO) in a client-side library.
    * Both implement _exactly the same_ interfaces.
* Jackson is much better/easier these days.
    * It's widely supported and does everything you could need.
* Most modeling can be achieved in POJO and these annotations:
    * @JsonDeserialize
    * @JsonTypeInfo
    * @JsonSubTypes

## Footnote

Author: **Andy Powney**

Jackson version: **2.9.4**

Further reading: https://github.com/FasterXML/jackson

