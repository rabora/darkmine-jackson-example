package org.darkmine.jackson.example.model.impl.pets;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import org.darkmine.jackson.example.model.Pet;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = PetCatImpl.class, name = "cat"),
        @JsonSubTypes.Type(value = PetDogImpl.class, name = "dog"),
        @JsonSubTypes.Type(value = PetDogImpl.class, name = "hound")
})
@Data
public abstract class AbstractPetImpl implements Pet {
    private String name;

    public abstract String getType();
}
