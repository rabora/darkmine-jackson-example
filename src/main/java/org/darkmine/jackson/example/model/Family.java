package org.darkmine.jackson.example.model;

import java.util.List;

public interface Family {
    List<Person> getPeople();
    String getSurname();
    List<Pet> getPets();
    Address getAddress();
}
