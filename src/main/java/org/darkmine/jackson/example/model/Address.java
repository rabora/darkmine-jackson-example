package org.darkmine.jackson.example.model;

public interface Address {
    String getLine1();
    String getLine2();
    String getLine3();
    String getLine4();
    String getPostCode();
    String getCountryCode();
}
