package org.darkmine.jackson.example.model.impl;

import lombok.Data;
import org.darkmine.jackson.example.model.Address;
import org.darkmine.jackson.example.model.Person;
import org.darkmine.jackson.example.model.Pet;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.darkmine.jackson.example.model.Family;
import org.darkmine.jackson.example.model.impl.pets.AbstractPetImpl;

import java.util.ArrayList;
import java.util.List;

@Data
public class FamilyImpl implements Family {

    @JsonDeserialize(as=List.class,contentAs=PersonImpl.class)
    private List<Person> people = new ArrayList<>();

    @JsonDeserialize(as=List.class,contentAs=AbstractPetImpl.class)
    private List<Pet> pets = new ArrayList<>();

    private String surname;

    @JsonDeserialize(as=AddressImpl.class)
    private Address address;
}
