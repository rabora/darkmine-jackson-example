package org.darkmine.jackson.example.model.impl;

import lombok.Data;
import org.darkmine.jackson.example.model.Address;

@Data
public class AddressImpl implements Address {
    private String line1;
    private String line2;
    private String line3;
    private String line4;
    private String postCode;
    private String countryCode;
}
