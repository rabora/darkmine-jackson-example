package org.darkmine.jackson.example.model.impl.pets;

public class PetCatImpl extends AbstractPetImpl {
    @Override
    public String getType() {
        return "cat";
    }
}
