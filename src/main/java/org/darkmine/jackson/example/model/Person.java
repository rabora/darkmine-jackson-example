package org.darkmine.jackson.example.model;

public interface Person {
    String getName();
    int getAge();
    Gender getGender();
}
