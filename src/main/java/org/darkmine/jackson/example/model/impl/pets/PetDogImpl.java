package org.darkmine.jackson.example.model.impl.pets;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PetDogImpl extends AbstractPetImpl {
    private boolean aggressive;
    @Override
    public String getType() {
        return "dog";
    }
}
