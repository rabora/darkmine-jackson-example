package org.darkmine.jackson.example.model.impl;

import lombok.Data;
import org.darkmine.jackson.example.model.Gender;
import org.darkmine.jackson.example.model.Person;

@Data
public class PersonImpl implements Person {
    private String name;
    private int age;
    private Gender gender;
}
