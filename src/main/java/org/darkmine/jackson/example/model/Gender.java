package org.darkmine.jackson.example.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum Gender {
    MALE("male","Man","Boy"),
    FEMALE("female","Woman","Girl");

    private final String code;
    private final String adultType;
    private final String childType;

    Gender(String code,String adultType,String childType) {
        this.code = code;
        this.adultType = adultType;
        this.childType = childType;
    }

    public String getCode() {
        return code;
    }
    public String getAdultType() {
        return adultType;
    }
    public String getChildType() {
        return childType;
    }

    /**
     * This is used by Jackson to create an instance of a Gender given a string of text. For larger enums, you should really
     * use a more efficient way of looking values up (such as in a Map).
     *
     * Exceptions thrown by this method will be wrapped in an IOException during parsing, but can be obtained via IOException.getCause().
     *
     * @param value The JSON value.
     * @return the value.
     */
    @JsonCreator
    public static Gender forValue(String value) {
        if( value == null ) return null;
        return Arrays.stream(Gender.values())
                .filter( (e) -> e.getCode().equals( value ) )
                .findFirst()
                .orElseThrow( () -> new IllegalArgumentException("Unable to parse '" + value + "'" ) );
    }

    /**
     * This is the value that gets written to the output stream when serialising into JSON.
     *
     * @return The JSON value.
     */
    @JsonValue
    public String toValue() {
        return getCode();
    }
}
