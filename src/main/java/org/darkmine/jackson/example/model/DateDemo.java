package org.darkmine.jackson.example.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.OptBoolean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.YearMonth;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class DateDemo {
    private Instant instant1;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant instant2;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Instant instant3;

    /**
     * NB. This is write-only, as yyyy-MM-dd cannot be directly transformed back into an Instant.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd",timezone = "UTC")
    private Instant instant4;

    private YearMonth yearMonth1;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private YearMonth yearMonth2;

    private LocalDateTime localDateTime1;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private LocalDateTime localDateTime2;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "GMT")
    private LocalDateTime localDateTime3;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private LocalDateTime localDateTime4;

    private Period period1;

    private Duration duration1;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Duration duration2;

}
