package org.darkmine.jackson.example.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.YearMonth;

import static org.junit.Assert.assertEquals;

public class DateDemoTest {

    private final Instant testInstant = Instant.ofEpochMilli(1563547164952L);
    private final YearMonth testYearMonth = YearMonth.of(2010,9);
    private final LocalDateTime testLocalDateTime = LocalDateTime.of(2010,9,21,16,31,57,123);
    private final Period testPeriod = Period.of(1,2,3);
    private final Duration testDuration = Duration.ofHours(3);
    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Test
    public void testEmptyObject() throws IOException {
        assertEquals("{}",
                objectMapper.writeValueAsString(DateDemo.builder().build())
        );
    }

    @Test
    public void testWriteInstant1() throws IOException {
        assertEquals("{\"instant1\":1563547164.952000000}",
                objectMapper.writeValueAsString(DateDemo.builder().instant1(testInstant).build())
        );
    }

    @Test
    public void testReadInstant1() throws IOException {
        assertEquals(DateDemo.builder().instant1(testInstant).build(),
                objectMapper.readValue("{\"instant1\":1563547164.952000000}",DateDemo.class)
        );
    }

    @Test
    public void testWriteInstant2() throws IOException {
        assertEquals("{\"instant2\":\"2019-07-19T14:39:24.952Z\"}",
                objectMapper.writeValueAsString(DateDemo.builder().instant2(testInstant).build())
        );
    }

    @Test
    public void testReadInstant2() throws IOException {
        assertEquals(DateDemo.builder().instant2(testInstant).build(),
                objectMapper.readValue("{\"instant2\":\"2019-07-19T14:39:24.952Z\"}", DateDemo.class)
        );
    }

    @Test
    public void testWriteInstant3() throws IOException {
        assertEquals("{\"instant3\":1563547164.952000000}",
                objectMapper.writeValueAsString(DateDemo.builder().instant3(testInstant).build())
        );
    }

    @Test
    public void testReadInstant3() throws IOException {
        assertEquals(DateDemo.builder().instant3(testInstant).build(),
                objectMapper.readValue("{\"instant3\":1563547164.952000000}", DateDemo.class)
        );
    }

    @Test
    public void testWriteInstant4() throws IOException {
        assertEquals("{\"instant4\":\"2019-07-19\"}",
                objectMapper.writeValueAsString(DateDemo.builder().instant4(testInstant).build())
        );
    }

    @Test(expected = MismatchedInputException.class)
    public void testReadInstant4() throws IOException {
        objectMapper.readValue("{\"instant4\":\"2019-07-19\"}", DateDemo.class);
    }

    @Test
    public void testWriteYearMonth1() throws IOException {
        assertEquals("{\"yearMonth1\":[2010,9]}",
                objectMapper.writeValueAsString(DateDemo.builder().yearMonth1(testYearMonth).build())
        );
    }

    @Test
    public void testReadYearMonth1() throws IOException {
        assertEquals(DateDemo.builder().yearMonth1(testYearMonth).build(),
                objectMapper.readValue("{\"yearMonth1\":[2010,9]}", DateDemo.class)
        );
    }

    @Test
    public void testWriteYearMonth2() throws IOException {
        assertEquals("{\"yearMonth2\":\"2010-09\"}",
                objectMapper.writeValueAsString(DateDemo.builder().yearMonth2(testYearMonth).build())
        );
    }

    @Test
    public void testReadYearMonth2() throws IOException {
        assertEquals(DateDemo.builder().yearMonth2(testYearMonth).build(),
                objectMapper.readValue("{\"yearMonth2\":\"2010-09\"}", DateDemo.class)
        );
    }

    @Test
    public void testWriteLocalDateTime1() throws IOException {
        assertEquals("{\"localDateTime1\":[2010,9,21,16,31,57,123]}",
                objectMapper.writeValueAsString(DateDemo.builder().localDateTime1(testLocalDateTime).build())
        );
    }

    @Test
    public void testReadLocalDateTime1() throws IOException {
        assertEquals(DateDemo.builder().localDateTime1(testLocalDateTime).build(),
                objectMapper.readValue("{\"localDateTime1\":[2010,9,21,16,31,57,123]}",DateDemo.class)
        );
    }

    @Test
    public void testWriteLocalDateTime2() throws IOException {
        assertEquals("{\"localDateTime2\":\"2010-09-21T16:31:57.000000123\"}",
                objectMapper.writeValueAsString(DateDemo.builder().localDateTime2(testLocalDateTime).build())
        );
    }

    @Test
    public void testReadLocalDateTime2() throws IOException {
        assertEquals(DateDemo.builder().localDateTime2(testLocalDateTime).build(),
                objectMapper.readValue("{\"localDateTime2\":\"2010-09-21T16:31:57.000000123\"}",DateDemo.class)
        );
    }

    @Test
    public void testWriteLocalDateTime3() throws IOException {
        assertEquals("{\"localDateTime3\":\"2010-09-21T16:31:57.000Z\"}",
                objectMapper.writeValueAsString(DateDemo.builder().localDateTime3(testLocalDateTime).build())
        );
    }

    @Test
    public void testReadLocalDateTime3() throws IOException {
        assertEquals(DateDemo.builder().localDateTime3(testLocalDateTime.withNano(0)).build(),
                objectMapper.readValue("{\"localDateTime3\":\"2010-09-21T16:31:57.000Z\"}",DateDemo.class)
        );
    }

    @Test
    public void testWriteLocalDateTime4() throws IOException {
        assertEquals("{\"localDateTime4\":\"2010-09-21T16:31:57.000\"}",
                objectMapper.writeValueAsString(DateDemo.builder().localDateTime4(testLocalDateTime).build())
        );
    }

    @Test
    public void testReadLocalDateTime4() throws IOException {
        assertEquals(DateDemo.builder().localDateTime4(testLocalDateTime.withNano(0)).build(),
                objectMapper.readValue("{\"localDateTime4\":\"2010-09-21T16:31:57.000\"}",DateDemo.class)
        );
    }

    @Test
    public void testWritePeriod1() throws IOException {
        assertEquals("{\"period1\":\"P1Y2M3D\"}",
                objectMapper.writeValueAsString(DateDemo.builder().period1(testPeriod).build())
        );
    }

    @Test(expected = MismatchedInputException.class)
    public void testReadPeriod1() throws IOException {
        objectMapper.readValue("{\"period1\":10800.000000000}",DateDemo.class);
    }

    @Test
    public void testWriteDuration1() throws IOException {
        assertEquals("{\"duration1\":10800.000000000}",
                objectMapper.writeValueAsString(DateDemo.builder().duration1(testDuration).build())
        );
    }

    @Test
    public void testReadDuration1() throws IOException {
        assertEquals(DateDemo.builder().duration1(testDuration).build(),
                objectMapper.readValue("{\"duration1\":10800.000000000}",DateDemo.class)
        );
    }

    @Test
    public void testWriteDuration2() throws IOException {
        assertEquals("{\"duration2\":\"PT3H\"}",
                objectMapper.writeValueAsString(DateDemo.builder().duration2(testDuration).build())
        );
    }

    @Test
    public void testReadDuration2() throws IOException {
        assertEquals(DateDemo.builder().duration2(testDuration).build(),
                objectMapper.readValue("{\"duration2\":\"PT3H\"}",DateDemo.class)
        );
    }
}
