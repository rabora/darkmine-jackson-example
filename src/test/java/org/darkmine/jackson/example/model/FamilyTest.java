package org.darkmine.jackson.example.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.darkmine.jackson.example.model.impl.*;
import org.darkmine.jackson.example.model.impl.pets.PetCatImpl;
import org.darkmine.jackson.example.model.impl.pets.PetDogImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class FamilyTest {

    private Person testPerson;
    private Pet testPetDog;
    private Pet testPetCat;
    private Address testAddress;

    @Before
    public void setup() {
        PersonImpl p = new PersonImpl();
        p.setAge( 55 );
        p.setName( "Fred" );
        testPerson = p;

        PetDogImpl dog = new PetDogImpl();
        dog.setName( "Fido" );
        testPetDog = dog;

        PetCatImpl cat = new PetCatImpl();
        cat.setName( "Monty" );
        testPetCat = cat;

        AddressImpl address = new AddressImpl();
        address.setLine1( "Buckingham Palace" );
        address.setLine2( "London" );
        address.setPostCode( "SW1A 1AA" );
        address.setCountryCode( "GBR" );
        testAddress = address;
    }

    @Test
    public void testSerialize() throws JsonProcessingException {
        FamilyImpl family = new FamilyImpl();
        family.getPeople().add( testPerson );
        family.setSurname( "Smith" );
        family.setAddress( testAddress );

        family.getPets().add( testPetCat );
        family.getPets().add( testPetDog );

        ObjectMapper om = new ObjectMapper();
        Assert.assertEquals( "{\"people\":[{\"name\":\"Fred\",\"age\":55,\"gender\":null}],\"pets\":[{\"name\":\"Monty\",\"type\":\"cat\"},{\"name\":\"Fido\",\"aggressive\":false," +
                "\"type\":\"dog\"}],\"surname\":\"Smith\",\"address\":{\"line1\":\"Buckingham Palace\",\"line2\":\"London\",\"line3\":null,\"line4\":null,\"postCode\":\"SW1A 1AA\",\"countryCode\":\"GBR\"}}" , om.writeValueAsString( family ) );
    }

    @Test
    public void testDeserialize() throws IOException {
        String json = "{\"people\":[{\"name\":\"Fred\",\"age\":55}],\"pets\":[{\"name\":\"Monty\",\"type\":\"cat\"},{\"name\":\"Fido\",\"aggressive\":true," +
                "\"type\":\"dog\"}],\"surname\":\"Smith\",\"address\":{\"line1\":\"Buckingham Palace\",\"line2\":\"London\",\"line3\":null," +
                "\"line4\":null,\"postCode\":\"SW1A 1AA\",\"countryCode\":\"GBR\"}}";
        ObjectMapper om = new ObjectMapper();
        Family family = om.readValue( json , FamilyImpl.class );
        Assert.assertEquals( "Smith" , family.getSurname() );
        Assert.assertEquals( 1 , family.getPeople().size() );
        Assert.assertEquals( 55 , family.getPeople().get(0).getAge() );
        Assert.assertEquals( "Fred" , family.getPeople().get(0).getName() );
        Assert.assertEquals( "Buckingham Palace" , family.getAddress().getLine1() );
        Assert.assertEquals( 2 , family.getPets().size() );
        family.getPets().stream()
                .filter( (pet) -> pet instanceof PetDogImpl )
                .map( (pet) -> (PetDogImpl)pet )
                .forEach( (dog) -> Assert.assertTrue(dog.isAggressive()) );
    }

    @Test
    public void testDeserializeWithAlias() throws IOException {
        String json = "{\"people\":[{\"name\":\"Fred\",\"age\":55}],\"pets\":[{\"name\":\"Monty\",\"type\":\"cat\"},{\"name\":\"Fido\",\"aggressive\":true," +
                "\"type\":\"hound\"}],\"surname\":\"Smith\",\"address\":{\"line1\":\"Buckingham Palace\",\"line2\":\"London\",\"line3\":null," +
                "\"line4\":null,\"postCode\":\"SW1A 1AA\",\"countryCode\":\"GBR\"}}";
        ObjectMapper om = new ObjectMapper();
        Family family = om.readValue( json , FamilyImpl.class );
        Assert.assertEquals( "Smith" , family.getSurname() );
        Assert.assertEquals( 1 , family.getPeople().size() );
        Assert.assertEquals( 55 , family.getPeople().get(0).getAge() );
        Assert.assertEquals( "Fred" , family.getPeople().get(0).getName() );
        Assert.assertEquals( "Buckingham Palace" , family.getAddress().getLine1() );
        Assert.assertEquals( 2 , family.getPets().size() );
        family.getPets().stream()
                .filter( (pet) -> pet instanceof PetDogImpl )
                .map( (pet) -> (PetDogImpl)pet )
                .forEach( (dog) -> Assert.assertTrue(dog.isAggressive()) );
    }
}
