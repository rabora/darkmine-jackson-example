package org.darkmine.jackson.example.model;

import org.darkmine.jackson.example.model.impl.PersonImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class PersonTest {

    @Test
    public void testSerialize() throws JsonProcessingException {
        PersonImpl person = new PersonImpl();
        person.setAge( 55 );
        person.setName( "Fred" );
        person.setGender( Gender.MALE );

        ObjectMapper om = new ObjectMapper();
        Assert.assertEquals( "{\"name\":\"Fred\",\"age\":55,\"gender\":\"male\"}" , om.writeValueAsString( person ) );
    }

    @Test
    public void testDeserialize() throws IOException {
        String json = "{\"name\":\"Sally\",\"age\":53,\"gender\":\"female\"}";
        ObjectMapper om = new ObjectMapper();
        Person person = om.readValue( json , PersonImpl.class );
        Assert.assertEquals( 53 , person.getAge() );
        Assert.assertEquals( "Sally" , person.getName() );
        Assert.assertEquals( Gender.FEMALE , person.getGender() );
    }
}
