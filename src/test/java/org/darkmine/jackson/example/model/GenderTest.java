package org.darkmine.jackson.example.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class GenderTest {

    private ObjectMapper om;

    @Before
    public void startup() {
        om = new ObjectMapper();
    }

    @Test
    public void testWriteJSON() throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        Gender demo = Gender.MALE;
        String json = om.writeValueAsString( demo );
        Assert.assertEquals( "\"male\"" , json );
    }

    @Test
    public void testReadJSON() throws IOException {
        ObjectMapper om = new ObjectMapper();
        String json = "\"female\"";
        Gender demo = om.readValue( json , Gender.class );
        Assert.assertEquals( Gender.FEMALE , demo );
    }

    @Test
    public void testReadInvalidJSON() {
        ObjectMapper om = new ObjectMapper();
        String json = "\"this is an illegal value\"";
        try {
            om.readValue(json, Gender.class);
            Assert.fail( "An exception should have been thrown" );
        } catch( IOException eee ) {
            Assert.assertEquals( IllegalArgumentException.class , eee.getCause().getClass() );
        }
    }

}
